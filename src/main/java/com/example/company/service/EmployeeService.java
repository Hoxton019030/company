package com.example.company.service;

import com.example.company.dao.EmployeeDao;
import com.example.company.entity.Employee;
import com.example.company.request.EmployeeAddRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Hoxton
 * @version 0.1.0
 * @since 0.1.0
 **/
@Service
@Transactional
public class EmployeeService {

    final EmployeeDao employeeDao;

    public EmployeeService(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }


    public void addEmployee(Employee employee){
        employeeDao.save(employee);
    }

    public Employee getEmployeeById(int id){
        return employeeDao.getEmployeeById(id);
    }





}
