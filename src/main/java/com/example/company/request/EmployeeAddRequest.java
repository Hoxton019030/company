package com.example.company.request;

import lombok.*;

/**
 * @author Hoxton
 * @version 0.1.0
 * @since 0.1.0
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeAddRequest {
    String name;
    int age;

}
