package com.example.company.controller;

import com.example.company.entity.Employee;
import com.example.company.request.EmployeeAddRequest;
import com.example.company.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Hoxton
 * @version 0.1.0
 * @since 0.1.0
 **/
//@Controller //用在JSP
@RestController //將資料轉成JSON格式
@RequestMapping("/Employee")
//@RequiredArgsConstructor
public class EmployeeController {

    final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/")
    public ResponseEntity<String > addEmployee(@RequestBody EmployeeAddRequest employeeAddRequest){
        String name = employeeAddRequest.getName();
        int age = employeeAddRequest.getAge();
        Employee employee = Employee.builder().name(name).age(age).build();


        employeeService.addEmployee(employee);
        return ResponseEntity.status(HttpStatus.OK).body("Creat Succeed");

    }

    @GetMapping("/{ididid}/{company}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("ididid") int id){
        Employee employee = employeeService.getEmployeeById(id);
        return ResponseEntity.status(HttpStatus.OK).body(employee);
    }

}
