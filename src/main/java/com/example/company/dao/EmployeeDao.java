package com.example.company.dao;

import com.example.company.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author Hoxton
 * @version 0.1.0
 * @since 0.1.0
 **/

@Repository //由SpringBoot 所創造的物件，就叫做SpringBean
public interface EmployeeDao extends JpaRepository<Employee,Integer>{


    @Query(value = "SELECT * FROM Employee WHERE id=:idddd",nativeQuery = true)
    public Employee getEmployeeById(@Param("idddd") int id);

    @Query(value = "DELETE FROM Employee WHERE id=:idddd",nativeQuery = true)
    @Modifying //CUD
    public void deleteEmployeeById(@Param("idddd") int id);

}
