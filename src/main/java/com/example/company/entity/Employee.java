package com.example.company.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @author Hoxton
 * @version 0.1.0
 * @since 0.1.0
 **/
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "name")
    String name;

    @Column(name = "age")
    int age;
}



